#!/bin/bash
echo "Hello the BETtery Application is about to start"

echo "Cloning frontend and backend"

(
  mkdir tmp && cd tmp
  git clone git@gitlab.com:paknecht1/bettery-backend.git

  git clone git@gitlab.com:paknecht1/bettery-frontend.git

  (
    echo "Building Backend"
    cd bettery-backend
    ./gradlew clean build
    docker build -t bettery-backend:prod .
  )

  (
    echo "Building Frontend"
    cd bettery-frontend
    docker build -t bettery-frontend:prod .
  )
)

rm -rf tmp

docker-compose up