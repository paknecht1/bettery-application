# Bettery Application

This project is to put together the whole BETtery-Application. 

This Application includes a Frontend (written in react), a Backend (written in java) and a Database (PostgreSQL).

With docker-compose it will start the whole application, as seen in this schema here: 

![Overview](/docs/ArchitectureDiagramm.png)

## Docker Compose

The docker compose file consists of the following images

* postgres
  * Database Storage
  * Same storage is used by the two backend instances
* bettery-backend 
  * Access to the database and providing a REST-Interface
  * Is deployed twice
  * Source repository (https://gitlab.com/paknecht1/bettery-backend)
* bettery-frontend
  * Static html/js files 
  * Provided by ngnix
  * Source repository (https://gitlab.com/paknecht1/bettery-frontend)
* web
  * ngnix server
  * does load-balancing
  * is also a reverse proxy. All /api calls are routed to bettery-backend the rest to bettery-frontend

### Startup Order 

The application will be started in the following order

1. Database
2. Bettery-frontend
3. Bettery-backend first instance
4. Bettery-backend second instance
5. Web with loadbalancer 

### Scale

To scale the application to run on more backends. Add another entry to the `docker-compose` like the `backendTwo` entry

## start.sh script

With ``start.sh`` you can install and start the newest version of the whole application.

